package com.montran.recruitment.enums;

public enum CvReviewStage {
	
	PENDING_FOR_CV_REVIEW,CV_ACCEPTED_BY_HR,CV_REJECTED_BY_HR

}
